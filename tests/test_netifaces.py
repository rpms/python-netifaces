import netifaces
import pytest


def test_interfaces():
    ifaces = netifaces.interfaces()
    assert isinstance(ifaces, list)
    assert "lo" in ifaces


@pytest.mark.parametrize(
    "iface,af,expected",
    [
        ("lo", netifaces.AF_INET, "127.0.0.1"),
        ("lo", netifaces.AF_INET6, "::1"),
    ],
)
def test_ifaddresses_lo(iface, af, expected):
    addrs = netifaces.ifaddresses(iface)
    assert addrs[af][0]["addr"] == expected


def test_invalid():
    with pytest.raises(ValueError):
        netifaces.ifaddresses("invalid interface")


def test_gateways():
    gw = netifaces.gateways()
    assert "default" in gw
